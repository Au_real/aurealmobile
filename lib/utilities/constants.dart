import 'package:flutter/material.dart';

const Color kActiveBorderColor = Color(0xFF085D7E);
const Color kInactiveBorderColor = Colors.transparent;
const Color kActiveTextColor = Color(0xFF191919);
const Color kInactiveTextColor = Color(0xFFCBCBCB);
