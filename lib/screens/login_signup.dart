import 'package:aureal/utilities/constants.dart';
import 'package:flutter/material.dart';

enum LoginState {
  login,
  signup,
}

enum SignupOption {
  mobile,
  email,
}

class Login extends StatefulWidget {
  static const String id = 'login_screen';

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  LoginState stateselector = LoginState.login;
  SignupOption signupSelector = SignupOption.email;
  String signupOptionText = 'Enter Your Email Address';
  bool userClickedNext = false;

  bool isThisYou = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            height: 90,
            child: Image.asset('assets/images/Favicon.png'),
          ),
          SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                          width: 3,
                          color: stateselector == LoginState.login
                              ? kActiveBorderColor
                              : kInactiveBorderColor), // add color here according to the flatbutton being pressed
                    ),
                  ),
                  child: FlatButton(
                    onPressed: () {
                      setState(() {
                        stateselector = LoginState.login;
                      });
                    },
                    child: Text(
                      'Login',
                      style: TextStyle(
                          fontSize: 20,
                          color: stateselector == LoginState.login
                              ? kActiveTextColor
                              : kInactiveTextColor),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        width: 3,
                        color: stateselector == LoginState.signup
                            ? kActiveBorderColor
                            : kInactiveBorderColor,
                      ),
                    ),
                  ),
                  child: FlatButton(
                    onPressed: () {
                      setState(() {
                        stateselector = LoginState.signup;
                      });
                    },
                    child: Text(
                      'SignUp',
                      style: TextStyle(
                          fontSize: 20,
                          color: stateselector == LoginState.signup
                              ? kActiveTextColor
                              : kInactiveTextColor),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 45,
          ),
          stateselector == LoginState.login ? login() : signUp(),
        ],
      ),
    );
  }

  Padding signUp() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: userClickedNext == false
          ? signUpEmailorPhone()
          : completeSignUp(), //replace with login or signup
    );
  }

  Column completeSignUp() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Text('Full Name'),
        ),
        TextField(
          onChanged: (value) {
            //Do something with the user input.
          },
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.person,
              color: kActiveBorderColor,
            ),
            hintText: 'Enter your name',
            contentPadding:
                EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: kActiveBorderColor, width: 1.0),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: kActiveBorderColor, width: 2.0),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Text('UserName'),
        ),
        TextField(
          onChanged: (value) {
            //Do something with the user input.
          },
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.account_circle,
              color: kActiveBorderColor,
            ),
            hintText: 'Enter your username',
            contentPadding:
                EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: kActiveBorderColor, width: 1.0),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: kActiveBorderColor, width: 2.0),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Text('Password'),
        ),
        TextField(
          onChanged: (value) {
            //Do something with the user input.
          },
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.lock,
              color: kActiveBorderColor,
            ),
            hintText: 'Enter your password',
            contentPadding:
                EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: kActiveBorderColor, width: 1.0),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: kActiveBorderColor, width: 2.0),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              gradient: LinearGradient(
                  colors: <Color>[Color(0xFF04587E), Color(0xFF34997E)])),
          child: MaterialButton(
            elevation: 20.0,
            onPressed: () {
              //Implement login functionality.
              setState(() {
                userClickedNext = true;
              });
            },
            minWidth: 200,
            height: 40.0,
            child: Text(
              'Sign Up',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        SizedBox(
          height: 180,
        ),
      ],
    );
  }

  Column signUpEmailorPhone() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              child: Container(
//                        decoration: BoxDecoration(
//                          border: Border(
//                            bottom: BorderSide(
//                                width: 3,
//                                color: signupSelector == SignupOption.email
//                                    ? kActiveBorderColor
//                                    : kInactiveBorderColor), // add color here according to the flatbutton being pressed
//                          ),
//                        ),
                child: FlatButton(
                  onPressed: () {
                    setState(() {
                      signupSelector = SignupOption.email;
                      signupOptionText = "Enter Your Email Address";
                    });
                  },
                  child: Text(
                    'Email',
                    style: TextStyle(
                        fontSize: 20,
                        color: signupSelector == SignupOption.email
                            ? kActiveTextColor
                            : kInactiveTextColor),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
//                        decoration: BoxDecoration(
//                          border: Border(
//                            bottom: BorderSide(
//                              width: 3,
//                              color: signupSelector == SignupOption.mobile
//                                  ? kActiveBorderColor
//                                  : kInactiveBorderColor,
//                            ),
//                          ),
//                        ),
                child: FlatButton(
                  onPressed: () {
                    setState(() {
                      signupSelector = SignupOption.mobile;
                      signupOptionText = "Enter Your Phone Number";
                    });
                  },
                  child: Text(
                    'Phone Number',
                    style: TextStyle(
                        fontSize: 20,
                        color: signupSelector == SignupOption.mobile
                            ? kActiveTextColor
                            : kInactiveTextColor),
                  ),
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 25,
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Text(signupOptionText),
        ),
        TextField(
          onChanged: (value) {
            //Do something with the user input.
          },
          decoration: InputDecoration(
            prefixIcon: Icon(
              signupOptionText == "Enter Your Email Address"
                  ? Icons.email
                  : Icons.phone,
              color: kActiveBorderColor,
            ),
            hintText: signupOptionText == "Enter Your Email Address"
                ? 'Email'
                : "Phone Number",
            contentPadding:
                EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: kActiveBorderColor, width: 1.0),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: kActiveBorderColor, width: 2.0),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              gradient: LinearGradient(
                  colors: <Color>[Color(0xFF04587E), Color(0xFF34997E)])),
          child: MaterialButton(
            elevation: 20.0,
            onPressed: () {
              //Implement login functionality.
              setState(() {
                userClickedNext = true;
              });
            },
            minWidth: 200,
            height: 40.0,
            child: Text(
              'Next',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        SizedBox(
          height: 80,
        ),
        Divider(
          height: 80,
          color: kActiveBorderColor,
          thickness: 1,
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: kActiveBorderColor),
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
          ),
          child: MaterialButton(
            highlightColor: kActiveBorderColor,
            onPressed: () {
              //Implement login functionality.
            },
            elevation: 5,
            minWidth: 200,
            height: 40.0,
            child: Text(
              'Sign Up with Steem',
              style: TextStyle(color: kActiveBorderColor),
            ),
          ),
        ),
        SizedBox(
          height: 40,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FlatButton(
              onPressed: () {}, //facebook Login API
              child: Container(
                height: 50,
                child: Image.asset('assets/images/facebook.png'),
              ),
            ),
            FlatButton(
              onPressed: () {}, //Google Login API
              child: Container(
                height: 50,
                child: Image.asset('assets/images/google.png'),
              ),
            ),
            FlatButton(
              onPressed: () {}, //Twitter Login API
              child: Container(
                height: 50,
                child: Image.asset('assets/images/twitter.png'),
              ),
            )
          ],
        ),
      ],
    );
  }

  Padding login() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: isThisYou == true ? thisIsme() : thisisntme(),
    );
  }

  Column thisisntme() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Text('Username or Email'),
        ),
        TextField(
          onChanged: (value) {
            //Do something with the user input.
          },
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.email,
              color: kActiveBorderColor,
            ),
            hintText: 'Enter your Username.',
            contentPadding:
                EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: kActiveBorderColor, width: 1.0),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: kActiveBorderColor, width: 2.0),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
          ),
        ),
        SizedBox(
          height: 25,
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Text('Password'),
        ),
        TextField(
          onChanged: (value) {
            //Do something with the user input.
          },
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.lock,
              color: kActiveBorderColor,
            ),
            hintText: 'Enter your password.',
            contentPadding:
                EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: kActiveBorderColor, width: 1.0),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: kActiveBorderColor, width: 2.0),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              gradient: LinearGradient(
                  colors: <Color>[Color(0xFF04587E), Color(0xFF34997E)])),
          child: MaterialButton(
            elevation: 20.0,
            onPressed: () {
              //Implement login functionality.
            },
            minWidth: 200,
            height: 40.0,
            child: Text(
              'Log In',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        SizedBox(
          height: 28,
        ),
        Text(
          'Forgot Password',
          style: TextStyle(color: kActiveBorderColor),
        ),
        Divider(
          height: 80,
          color: kActiveBorderColor,
          thickness: 1,
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: kActiveBorderColor),
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
          ),
          child: MaterialButton(
            highlightColor: kActiveBorderColor,
            onPressed: () {
              //Implement login functionality.
            },
            elevation: 5,
            minWidth: 200,
            height: 40.0,
            child: Text(
              'Sign In with Steem',
              style: TextStyle(color: kActiveBorderColor),
            ),
          ),
        ),
        SizedBox(
          height: 40,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FlatButton(
              onPressed: () {}, //facebook Login API
              child: Container(
                height: 50,
                child: Image.asset('assets/images/facebook.png'),
              ),
            ),
            FlatButton(
              onPressed: () {}, //Google Login API
              child: Container(
                height: 50,
                child: Image.asset('assets/images/google.png'),
              ),
            ),
            FlatButton(
              onPressed: () {}, //Twitter Login API
              child: Container(
                height: 50,
                child: Image.asset('assets/images/twitter.png'),
              ),
            )
          ],
        ),
      ],
    );
  }

  Column thisIsme() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Hello Again!',
                  style: TextStyle(fontSize: 20),
                ), //Add styles to individual Texts
                Text(
                  'Shubham',
                  style: TextStyle(fontSize: 28),
                ),
                FlatButton(
                  onPressed: () {
                    setState(() {
                      isThisYou = false;
                    });
                  },
                  child: Text(
                    "This isn't me",
                    style: TextStyle(color: kActiveBorderColor),
                  ),
                ),
              ],
            ),
            CircleAvatar(
              radius: 40,
              backgroundColor: kActiveBorderColor,
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Text('Password'),
        ),
        TextField(
          onChanged: (value) {
            //Do something with the user input.
          },
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.lock,
              color: kActiveBorderColor,
            ),
            hintText: 'Enter your password.',
            contentPadding:
                EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: kActiveBorderColor, width: 1.0),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: kActiveBorderColor, width: 2.0),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              gradient: LinearGradient(
                  colors: <Color>[Color(0xFF04587E), Color(0xFF34997E)])),
          child: MaterialButton(
            elevation: 20.0,
            onPressed: () {
              //Implement login functionality.
            },
            minWidth: 200,
            height: 40.0,
            child: Text(
              'Log In',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        SizedBox(
          height: 28,
        ),
        Text(
          'Forgot Password',
          style: TextStyle(color: kActiveBorderColor),
        ),
        Divider(
          height: 80,
          color: kActiveBorderColor,
          thickness: 1,
        ),
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: kActiveBorderColor),
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
          ),
          child: MaterialButton(
            highlightColor: kActiveBorderColor,
            onPressed: () {
              //Implement login functionality.
            },
            elevation: 5,
            minWidth: 200,
            height: 40.0,
            child: Text(
              'Sign In with Steem',
              style: TextStyle(color: kActiveBorderColor),
            ),
          ),
        ),
        SizedBox(
          height: 40,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FlatButton(
              onPressed: () {}, //facebook Login API
              child: Container(
                height: 50,
                child: Image.asset('assets/images/facebook.png'),
              ),
            ),
            FlatButton(
              onPressed: () {}, //Google Login API
              child: Container(
                height: 50,
                child: Image.asset('assets/images/google.png'),
              ),
            ),
            FlatButton(
              onPressed: () {}, //Twitter Login API
              child: Container(
                height: 50,
                child: Image.asset('assets/images/twitter.png'),
              ),
            )
          ],
        ),
      ],
    );
  }
}
